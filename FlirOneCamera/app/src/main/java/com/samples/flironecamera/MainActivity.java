/*
 * ******************************************************************
 * @title FLIR THERMAL SDK
 * @file MainActivity.java
 * @Author FLIR Systems AB
 *
 * @brief  Main UI of test application
 *
 * Copyright 2019:    FLIR Systems
 * ******************************************************************/
package com.samples.flironecamera;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.flir.thermalsdk.ErrorCode;
import com.flir.thermalsdk.androidsdk.ThermalSdkAndroid;
import com.flir.thermalsdk.androidsdk.live.connectivity.UsbPermissionHandler;
import com.flir.thermalsdk.live.CommunicationInterface;
import com.flir.thermalsdk.live.Identity;
import com.flir.thermalsdk.live.connectivity.ConnectionStatus;
import com.flir.thermalsdk.live.connectivity.ConnectionStatusListener;
import com.flir.thermalsdk.live.discovery.DiscoveryEventListener;
import com.flir.thermalsdk.log.ThermalLog;
import org.jetbrains.annotations.NotNull;
import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

/**
 * Sample application for scanning a FLIR ONE or a built in emulator
 * <p>
 * See the {@link CameraHandler} for how to preform discovery of a FLIR ONE camera, connecting to it and start streaming images
 * <p>
 * The MainActivity is primarily focused to "glue" different helper classes together and updating the UI components
 * <p/>
 * Please note, this is <b>NOT</b> production quality code, error handling has been kept to a minimum to keep the code as clear and concise as possible
 */
public class MainActivity extends AppCompatActivity {

	private static final String TAG = "MainActivity";

	//Handles Android permission for eg Network
	private PermissionHandler permissionHandler;

	//Handles network camera operations
	private CameraHandler cameraHandler;

	private Identity connectedIdentity = null;
	private TextView connectionStatus;
	private TextView discoveryStatus;

	private ImageView msxImage;
	private ImageView photoImage;

	private LinkedBlockingQueue<FrameDataHolder> framesBuffer = new LinkedBlockingQueue(21);
	private UsbPermissionHandler usbPermissionHandler = new UsbPermissionHandler();


	/**
	 * Show message on the screen
	 */
	public interface ShowMessage {
		void show(String message);
	}

	private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
	Button sendBtn;
	Button autoBtn;
	EditText txtphoneNo;
	EditText txtMessage;
	TextView tempDisp;
	String phoneNo;
	String message;
	String finalMessage;
	String tempMsg;
	EditText threshold;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//		NotificationManager mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		sendBtn = (Button) findViewById(R.id.alertButton);
		autoBtn = (Button) findViewById(R.id.autoButton);
		txtphoneNo = (EditText) findViewById(R.id.sendNumber);
		txtMessage = (EditText) findViewById(R.id.sendMessage);
		tempDisp = (TextView) findViewById(R.id.tempDisp);
		threshold = (EditText) findViewById(R.id.threshold);
//		public static BluetoothClass.Device.BatteryChargingState get(int value);

		sendBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				sendSMSMessage();
			}
		});

		autoBtn.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view) {
				cameraHandler.setThresh(Double.valueOf(threshold.getText().toString()));
				Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
				r.play();

				Handler handler = new Handler();
				int delay = 100; //milliseconds

				handler.postDelayed(new Runnable(){
					public void run(){
						tempDisp.setText(cameraHandler.getTempats() + "°F");
						handler.postDelayed(this, delay);
					}
				}, delay);

				//reset max counter every 5 seconds
				Handler handler2 = new Handler();
				int delay2 = 5000; //milliseconds

				handler.postDelayed(new Runnable(){
					public void run(){
						//send sms
						if(cameraHandler.getTempatd() > cameraHandler.getThresh()) {
							//call notification
							r.play();
							sendSMSMessage();
							Bitmap sendableMSX = cameraHandler.getMsxBitmap();
							r.play();
						}
						cameraHandler.setTempatd(0.01);
						handler.postDelayed(this, delay2);
					}
				}, delay);



			}
		});

		//notification player
//		public void showHeatNotification() {
//			Log.i("start", "notification");
//			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.this);
//			mBuilder.setSound(alarmSound);
//			Intent intnt = new Intent(MainActivity.this, NotifActivity.class);
//			PendingIntent pendint = PendingIntent.getActivity(MainActivity.this,0,intnt,PendingIntent.FLAG_UPDATE_CURRENT);
//			mBuilder.setContentIntent(pendint);
//			NotificationManager notmana = (NotificationManager)getSystemService(
//				Context.NOTIFICATION_SERVICE);
//			notmana.notify(0,mBuilder.build());
//		};


		ThermalLog.LogLevel enableLoggingInDebug = BuildConfig.DEBUG ? ThermalLog.LogLevel.DEBUG : ThermalLog.LogLevel.NONE;

		//ThermalSdkAndroid has to be initiated from a Activity with the Application Context to prevent leaking Context,
		// and before ANY using any ThermalSdkAndroid functions
		//ThermalLog will show log from the Thermal SDK in standards android log framework
		ThermalSdkAndroid.init(getApplicationContext(), enableLoggingInDebug);

		permissionHandler = new PermissionHandler(showMessage, MainActivity.this);

		cameraHandler = new CameraHandler();

		setupViews();

		showSDKversion(ThermalSdkAndroid.getVersion());
	}


	public void startDiscovery(View view) {
		startDiscovery();
	}

	public void stopDiscovery(View view) {
		stopDiscovery();
	}


	public void connectFlirOne(View view) {
		connect(cameraHandler.getFlirOne());
	}

	public void connectSimulatorOne(View view) {
		connect(cameraHandler.getCppEmulator());
	}

	public void connectSimulatorTwo(View view) {
		connect(cameraHandler.getFlirOneEmulator());
	}

	public void disconnect(View view) {
		disconnect();
	}


	public void sendSMSMessage() {
		phoneNo = txtphoneNo.getText().toString();
		message = txtMessage.getText().toString();
		tempMsg = " " + cameraHandler.getTempats();
		finalMessage = message += tempMsg += "°F";

		if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
			if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.SEND_SMS)) {
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, MY_PERMISSIONS_REQUEST_SEND_SMS);
			}
		} else {
			//clearApplicationData();
			//Then without clearing cache..
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(phoneNo, null, message, null, null);
				Toast.makeText(getApplicationContext(), "Secondary SMS Sent.", Toast.LENGTH_LONG).show();
		}
	}

	//for clearing cache
//	public void clearApplicationData() {
//		File cache = getCacheDir();
//		File appDir = new File(cache.getParent());
//		if (appDir.exists()) {
//			String[] children = appDir.list();
//			for (String s : children) {
//				if (!s.equals("lib")) {
//					deleteDir(new File(appDir, s));
//					Log.i("EEEEEERRRRRRROOOORRRR", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
//				}
//			}
//		}
//	}
//	public static boolean deleteDir(File dir) {
//		if (dir != null && dir.isDirectory()) {
//			String[] children = dir.list();
//			for (int i = 0; i < children.length; i++) {
//				boolean success = deleteDir(new File(dir, children[i]));
//				if (!success) {
//					return false;
//				}
//			}
//		}
//		return dir.delete();
//	}
// end clearing cache

//		SmsManager smsManager = SmsManager.getDefault();
//		smsManager.sendTextMessage(phoneNumber, null, message, null, null);
//		Toast.makeText(this, "Message Sent", Toast.LENGTH_SHORT).show();

//	@Override
//	public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
//		switch (requestCode) {
//			case MY_PERMISSIONS_REQUEST_SEND_SMS: {
//				if (grantResults.length > 0
//					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//					SmsManager smsManager = SmsManager.getDefault();
//					smsManager.sendTextMessage(phoneNo, null, message, null, null);
//					Toast.makeText(getApplicationContext(), "SMS sent.",
//						Toast.LENGTH_LONG).show();
//				} else {
//					Toast.makeText(getApplicationContext(),
//						"SMS faild, please try again.", Toast.LENGTH_LONG).show();
//					return;
//				}
//			}
//		}
//
//	}

    /**
     * Handle Android permission request response for Bluetooth permissions & SMS
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        Log.d(TAG, "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
//        permissionHandler.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //doubled up
		switch (requestCode) {
			case MY_PERMISSIONS_REQUEST_SEND_SMS: {
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					SmsManager smsManager = SmsManager.getDefault();
					smsManager.sendTextMessage(phoneNo, null, finalMessage, null, null);
					Toast.makeText(getApplicationContext(), "SMS sent.", Toast.LENGTH_LONG).show();
					return;
				} else {
					Toast.makeText(getApplicationContext(),"SMS faild, please try again.", Toast.LENGTH_LONG).show();
					return;
				}
			}
		}
    }

    /**
     * Connect to a Camera
     */
    private void connect(Identity identity) {
        //We don't have to stop a discovery but it's nice to do if we have found the camera that we are looking for
        cameraHandler.stopDiscovery(discoveryStatusListener);

        if (connectedIdentity != null) {
            Log.d(TAG, "connect(), in *this* code sample we only support one camera connection at the time");
            showMessage.show("connect(), in *this* code sample we only support one camera connection at the time");
            return;
        }

        if (identity == null) {
            Log.d(TAG, "connect(), can't connect, no camera available");
            showMessage.show("connect(), can't connect, no camera available");
            return;
        }

        connectedIdentity = identity;

        updateConnectionText(identity, ConnectionStatus.CONNECTING);
        //IF your using "USB_DEVICE_ATTACHED" and "usb-device vendor-id" in the Android Manifest
        // you don't need to request permission, see documentation for more information
        if (UsbPermissionHandler.isFlirOne(identity)) {
            usbPermissionHandler.requestFlirOnePermisson(identity, this, permissionListener);
        } else {
            cameraHandler.connect(identity, connectionStatusListener);
        }

    }

    private UsbPermissionHandler.UsbPermissionListener permissionListener = new UsbPermissionHandler.UsbPermissionListener() {
        @Override
        public void permissionGranted(Identity identity) {
            cameraHandler.connect(identity, connectionStatusListener);
        }

        @Override
        public void permissionDenied(Identity identity) {
            MainActivity.this.showMessage.show("Permission was denied for identity ");
        }

        @Override
        public void error(UsbPermissionHandler.UsbPermissionListener.ErrorType errorType, final Identity identity) {
            MainActivity.this.showMessage.show("Error when asking for permission for FLIR ONE, error:"+errorType+ " identity:" +identity);
        }
    };

    /**
     * Disconnect to a camera
     */
    private void disconnect() {
        connectedIdentity = null;
        Log.d(TAG, "disconnect() called with: connectedIdentity = [" + connectedIdentity + "]");
        cameraHandler.disconnect();
    }

    /**
     * Update the UI text for connection status
     */
    private void updateConnectionText(Identity identity, ConnectionStatus status) {
        String deviceId = identity != null ? identity.deviceId : "";
        connectionStatus.setText(getString(R.string.connection_status_text, deviceId + " " + status));
    }

    /**
     * Start camera discovery
     */
    private void startDiscovery() {
        cameraHandler.startDiscovery(cameraDiscoveryListener, discoveryStatusListener);
    }

    /**
     * Stop camera discovery
     */
    private void stopDiscovery() {
        cameraHandler.stopDiscovery(discoveryStatusListener);
    }

    /**
     * Callback for discovery status, using it to update UI
     */
    private CameraHandler.DiscoveryStatus discoveryStatusListener = new CameraHandler.DiscoveryStatus() {
        @Override
        public void started() {
            discoveryStatus.setText(getString(R.string.connection_status_text, "Discovering"));
        }

        @Override
        public void stopped() {
            discoveryStatus.setText(getString(R.string.connection_status_text, "Not Discovering"));
        }
    };

    /**
     * Camera connecting state thermalImageStreamListener, keeps track of if the camera is connected or not
     * <p>
     * Note that callbacks are received on a non-ui thread so have to eg use {@link #runOnUiThread(Runnable)} to interact view UI components
     */
    private ConnectionStatusListener connectionStatusListener = new ConnectionStatusListener() {
        @Override
        public void onConnectionStatusChanged(@NotNull ConnectionStatus connectionStatus, @org.jetbrains.annotations.Nullable ErrorCode errorCode) {
            Log.d(TAG, "onConnectionStatusChanged connectionStatus:" + connectionStatus + " errorCode:" + errorCode);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateConnectionText(connectedIdentity, connectionStatus);

                    switch (connectionStatus) {
                        case CONNECTING: break;
                        case CONNECTED: {
                            cameraHandler.startStream(streamDataListener);
                        }
                        break;
                        case DISCONNECTING: break;
                        case DISCONNECTED: break;
                    }
                }
            });
        }
    };

    private final CameraHandler.StreamDataListener streamDataListener = new CameraHandler.StreamDataListener() {

        @Override
        public void images(FrameDataHolder dataHolder) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    msxImage.setImageBitmap(dataHolder.msxBitmap);
                    photoImage.setImageBitmap(dataHolder.dcBitmap);

                }
            });
        }

        @Override
        public void images(Bitmap msxBitmap, Bitmap dcBitmap) {

            try {
                framesBuffer.put(new FrameDataHolder(msxBitmap,dcBitmap));
            } catch (InterruptedException e) {
                //if interrupted while waiting for adding a new item in the queue
                Log.e(TAG,"images(), unable to add incoming images to frames buffer, exception:"+e);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.d(TAG,"framebuffer size:"+framesBuffer.size());
                    FrameDataHolder poll = framesBuffer.poll();
                    msxImage.setImageBitmap(poll.msxBitmap);
                    photoImage.setImageBitmap(poll.dcBitmap);
                }
            });

        }
    };

    /**
     * Camera Discovery thermalImageStreamListener, is notified if a new camera was found during a active discovery phase
     * <p>
     * Note that callbacks are received on a non-ui thread so have to eg use {@link #runOnUiThread(Runnable)} to interact view UI components
     */
    private DiscoveryEventListener cameraDiscoveryListener = new DiscoveryEventListener() {
        @Override
        public void onCameraFound(Identity identity) {
            Log.d(TAG, "onCameraFound identity:" + identity);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    cameraHandler.add(identity);
                }
            });
        }

        @Override
        public void onDiscoveryError(CommunicationInterface communicationInterface, ErrorCode errorCode) {
            Log.d(TAG, "onDiscoveryError communicationInterface:" + communicationInterface + " errorCode:" + errorCode);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    stopDiscovery();
                    MainActivity.this.showMessage.show("onDiscoveryError communicationInterface:" + communicationInterface + " errorCode:" + errorCode);
                }
            });
        }
    };

    private ShowMessage showMessage = new ShowMessage() {
        @Override
        public void show(String message) {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
        }
    };

    private void showSDKversion(String version) {
        TextView sdkVersionTextView = findViewById(R.id.sdk_version);
        String sdkVersionText = getString(R.string.sdk_version_text, version);
        sdkVersionTextView.setText(sdkVersionText);
    }

    private void setupViews() {
        connectionStatus = findViewById(R.id.connection_status_text);
        discoveryStatus = findViewById(R.id.discovery_status);

        msxImage = findViewById(R.id.msx_image);
        photoImage = findViewById(R.id.photo_image);
    }

}

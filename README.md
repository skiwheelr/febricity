# febricity

Mobile Thermal Imaging system for Harm Reduction, Disease Prevention for use at live events.

The camViewBasic directory is a complete android project for building an apk from scratch,
which will import the thermal viewing functionality from the FLIR SDK.

Once the import and configuration has been demonstrated to work,
a new project will be created.

Project is awaiting a developer license which will allow development based off one of the existing FLIR certified apps.

I would like to keep it to a master franch for now and once a milestone is hit, start a develop branch from the master.

